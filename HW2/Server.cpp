#include "Server.h"
#include <ws2tcpip.h>
#include <vector>

std::vector<std::thread*> vt; //Threads saved in vector for ahead (if we'll need to join them or access them for a reason)

Server::Server(std::string Port) : _details(NULL)
{
	WSAStartup(MAKEWORD(2, 0), &this->_wsaData);

	struct addrinfo hints;
	this->_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (_socket < 0)
		throw "Invalid socket";
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	if (getaddrinfo(NULL, Port.c_str(), &hints, &this->_details) != 0)
	{
		throw "GetAddrInfo failed";
	}

	if ( (bind(this->_socket, this->_details->ai_addr, this->_details->ai_addrlen)) != 0)
	{
		throw "Bind Unsuccessful";
	}

	freeaddrinfo(this->_details);

	while (true) //Always open
	{
		this->AcceptConnections();
	}

	std::cout << "Cleanup." << std::endl;
	WSACleanup(); //Cleanup
}

void Server::AcceptConnections(void)
{
	if (listen(this->_socket, SOMAXCONN) != 0) //Listening
		throw "Listen Unsuccessful";
	char buf[1025] = { 0 };
	char* ans = "Accepted";
	SOCKET client;
	struct sockaddr_in client_addr;
	int addrlen = sizeof(client_addr);
		
	client = accept(this->_socket, (struct sockaddr*)&client_addr, &addrlen); //Accepting connection
	if (client != INVALID_SOCKET)
	{
		std::thread* t = new std::thread(&Server::AcceptConnections, this);
		t->detach(); //Detaching thread
		vt.push_back(t);
	}
	else
	{
		throw "Accept Not Working";
	}

	getsockname(client, (struct sockaddr*)&client_addr, &addrlen); //Getting details over client

	std::cout << int(client_addr.sin_addr.S_un.S_addr & 0xFF) << "." << int((client_addr.sin_addr.S_un.S_addr & 0xFF00) >> 8)
		<< "." << int((client_addr.sin_addr.S_un.S_addr & 0xFF0000)>>16) << "." << int((client_addr.sin_addr.S_un.S_addr & 0xFF000000)>>24); //printing IP
	std::cout << ":" << ntohs(client_addr.sin_port) << " - Connection Accepted." << std::endl; //printing IP and port of client.

	while (strncmp("Exit", buf, 5))
	{
		int recvBytes = this->Receive(client, buf, MY_MAX_LEN); //Receiving message
		if (recvBytes <= 0) //Validating
		{
			std::cout << "Error: " << WSAGetLastError() << std::endl;
			throw "Client disconnected";
		}
		buf[recvBytes] = '\0';
		if (strncmp(buf, "Exit", 5))
		{
			std::cout << "Message recieved from client " << GetCurrentThreadId() << " is: \"" << buf << "\"" << std::endl;
			std::cout << std::endl;

			std::cout << "Sending to client " << GetCurrentThreadId() << ": \"" << ans << "\"." << std::endl;
			this->Send(client, ans, strlen(ans));
			std::cout << "\"" << ans << "\" sent." << std::endl;
			std::cout << std::endl;
		}
		else
		{
			std::cout << "Exit sent by client " << GetCurrentThreadId() << ". Closing socket " << client << std::endl;
		}
	}

	std::cout << std::endl;
	std::cout << "Closing connection." << std::endl;
	std::cout << std::endl << std::endl;
	closesocket(client);
}

int Server::Send(SOCKET ClientSocket, void* buf, int len, int flags)
{
	send(ClientSocket, (const char*)&len, sizeof(int), 0); //sending the length of the message
	return send(ClientSocket, (const char*)buf, len, flags); //sending the message itself
}

int Server::Receive(SOCKET ClientSocket, void* buf, int maxlen, int flags)
{
	int len_to_recv = 0;
	recv(ClientSocket, (char*)&len_to_recv, sizeof(int), 0); //Receiving the length of the message first
	if (maxlen != 0 && len_to_recv > maxlen) //0 symbolizes unlimited len and we check if there's not too much information
	{
		throw "Receive: Too much information";
	}

	return recv(ClientSocket, (char*)buf, len_to_recv, flags);
}

Server::~Server()
{
	for (std::vector<std::thread*>::iterator it = vt.begin(); it != vt.end(); it++)
	{
		(*it)->join();
		free(*it);
	}
}