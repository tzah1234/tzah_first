#include "FileManager.h"

FileManager::FileManager(string filename) : _num_of_readers(0),  _num_of_writers(0)
{
	_f.open(filename.c_str(), ios::in | ios::out); //Both reading and writing
	if(_f.bad())
		throw "BAD FILE";
}

void FileManager::write()
{
	_num_of_writers++;
	_cond.wait(unique_lock<mutex>(_mtx_accessing), [&](){ return _num_of_readers == 0; }); //Waiting for a situation where there are no readers.
	_mtx_accessing.lock(); //Locking the access mutex

	cout << "WRITING" << endl;

	_mtx_accessing.unlock(); //Unlocking the access mutex
	_num_of_writers--;
	if(_num_of_readers == 0)
	{
		_cond.notify_one(); //Notify another writer
	}
}

void FileManager::read()
{
	_num_of_readers++;
	_mtx_accessing.lock(); //Locking the access mutex

	cout << "READING" << endl;

	_mtx_accessing.unlock(); //Unlocking the access mutex
	_num_of_readers--;
	if(_num_of_readers == 0)
	{
		_cond.notify_one();
	}
}

FileManager::~FileManager(void)
{
	_f.close();
}
