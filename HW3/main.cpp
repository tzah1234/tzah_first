#include "FileManager.h"
#include <vector>

FileManager fm(""); //put here
vector<thread*> vt;

void my_read()
{
	fm.read();
}

void my_write()
{
	fm.write();
}

int main()
{
	srand(time(NULL));
	for(int i = 0 ; i < 20 ; i++)
	{
		bool read = (rand() % 2);
		thread* t;
		if(read)
		{
			t = new thread(&my_read);
		}
		else
		{
			t = new thread(&my_write);
		}
		vt.push_back(t);
		//this_thread::sleep_for(chrono::seconds(3)); //Uncomment this line -> the notifications would random (not in order, just as threaded)
	}
	
	for(int i = 0 ; i < vt.size() ; i++)
		vt[i]->join();

	system("pause");

	return 0;
}
