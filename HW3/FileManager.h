#pragma once
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <mutex>
#include <condition_variable>

using namespace std;

class FileManager
{
public:
	FileManager(string filename);
	void write(); //Writing function
	void read(); //Reading function
	~FileManager(void);
private:
	fstream _f; //The stream we use to write to a file
	int _num_of_readers; //Number of readers
	int _num_of_writers; //Number of writers
	condition_variable _cond; //Cond that writers wait on
	mutex _mtx_accessing; //Mutex for accessing the file
};

