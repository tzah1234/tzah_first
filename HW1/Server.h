#pragma once
#define MY_MAX_LEN 1024
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
using namespace std;
class Server
{
public:
	Server(string Port);
	void AcceptConnections();
	int Send(void* buf, int len, int flags = 0);
	int Receive(void* buf, int max_len, int flags = 0);
	~Server();
private:
	SOCKET _socket;
	struct addrinfo* _details;
	WSADATA _wsaData;
};