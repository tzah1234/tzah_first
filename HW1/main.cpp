#include "Server.h"
#include <thread>
#include <string>
#define PORT "7777"
#define SERVER_ADDR "127.0.0.1"

#pragma comment(lib, "Ws2_32.lib")

int main()
{
	Server s(PORT);

	cout << endl << endl;
	system("pause");
	return 0;
}