#include "Server.h"
#include <ws2tcpip.h>
#include <vector>
#include <thread>
#include "TakiProtocol.h"
#include "Message.h"
#include "User.h"
#include "Manager.h"
#include <string>
#include <mutex>
#include <set>
#include <stdafx.h>

std::vector<std::thread*> vt; //Threads saved in vector for ahead (if we'll need to join them or access them for a reason)
set<User*, bool(*)(User*, User*)> vu([](User* a, User* b){ return (bool)(*a < *b); }); //Current connected users vector

mutex mtx_vu;

#define USEC_TO_WAIT 100000
#define NEXT_PLAYER(a) a->getPlayers()[(a->getCurrentPlayer() + 1) % a->getPlayers().size()]

Server::Server(std::string Port) : _details(NULL)
{
	WSAStartup(MAKEWORD(2, 0), &this->_wsaData);

	struct addrinfo hints;
	this->_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (_socket < 0)
		throw "Invalid socket";
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;


	if (getaddrinfo(NULL, Port.c_str(), &hints, &this->_details) != 0)
	{
		throw "GetAddrInfo failed";
	}

	if ((::bind(this->_socket, this->_details->ai_addr, this->_details->ai_addrlen)) != 0)
	{
		throw "Bind Unsuccessful";
	}

	freeaddrinfo(this->_details);

	if(!Manager::init_db())
		throw "Database not successfuly initialized.";

	this->foo();

	std::cout << "Cleanup." << std::endl;
	WSACleanup(); //Cleanup
}

Server::Server(const Server& other)
{
}

void Server::foo()
{
	while (true)
	{
		if (listen(this->_socket, SOMAXCONN) != 0) //Listening
			throw "Listen Unsuccessful";
		SOCKET client;
		User* user = NULL;
		struct sockaddr_in client_addr;
		int addrlen = sizeof(client_addr);
		char* buf = new char[MY_MAX_LEN + 1];

		client = accept(this->_socket, (struct sockaddr*)&client_addr, &addrlen); //Accepting connection
		if (client != INVALID_SOCKET)
		{
			std::thread* t = new std::thread(&Server::AcceptConnections, this, client);
			vt.push_back(t);

			srand((unsigned int)time(NULL));
		}
		else
		{
			throw "Accept Not Working";
		}
	}
}

void Server::AcceptConnections(SOCKET client)
{
	User* user = NULL;
	struct sockaddr_in client_addr;
	int addrlen = sizeof(client_addr);
	char* buf = new char[MY_MAX_LEN + 1];

	getsockname(client, (struct sockaddr*)&client_addr, &addrlen); //Getting details over client

	std::cout << int(client_addr.sin_addr.S_un.S_addr & 0xFF) << "." << int((client_addr.sin_addr.S_un.S_addr & 0xFF00) >> 8)
		<< "." << int((client_addr.sin_addr.S_un.S_addr & 0xFF0000) >> 16) << "." << int((client_addr.sin_addr.S_un.S_addr & 0xFF000000) >> 24); //printing IP
	std::cout << ":" << ntohs(client_addr.sin_port) << " - Connection Accepted." << std::endl; //printing IP and port of client.

	HandleClient(client);

	std::cout << std::endl;
	std::cout << "Closing connection." << std::endl;
	std::cout << std::endl << std::endl;
	closesocket(client);
}

bool Server::LoginClient(User** user, SOCKET client) //Note: maybe bool
{
	char* buf = new char[1025];
	string str;
	Message* msg;
	vector<string> vs;
	bool okay = false, shouldBreak = false;

	while (!okay)
	{
		str = "";
		int err = this->Receive(client, (char*)buf, MY_MAX_LEN);
		if (err == -1)
		{
			return false;
		}
		else if(err == 0)
		{
			continue;
		}
		msg = new Message(string(buf));

		if (!msg->check_validity())
		{
			str += MESSAGE(PGM_MER_MESSAGE) + MSG_ARG_DELIM;

			this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
		}
		else if ((!(msg->getCodeNumber() == EN_LOGIN || msg->getCodeNumber() == EN_REGISTER)))
		{
			str += MESSAGE(PGM_MER_ACCESS) + MSG_ARG_DELIM;

			this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
		}
		else
		{
			vs = msg->getMsgStrings();
			if (vs.size() != 2)
			{
				str += MESSAGE(PGM_MER_MESSAGE) + MSG_ARG_DELIM;

				this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
				continue;
			}
			*user = new User(vs[0], vs[1], client);
			if (!Manager::is_exist(**user, false) && msg->getCodeNumber() == EN_LOGIN)
			{
				str += MESSAGE(PGM_ERR_LOGIN) + MSG_ARG_DELIM;

				this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
			}
			else if (Manager::is_exist(**user, true) && msg->getCodeNumber() == EN_REGISTER)
			{
				str += MESSAGE(PGM_ERR_NAME_TAKEN) + msg->getMsgStrings()[0] + MESSAGE_END();

				this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
			}
			else if (vu.find(*user) != vu.end())
			{
				str += MESSAGE(PGM_ERR_LOGIN) + MSG_ARG_DELIM;

				this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
			}
			else
			{
				if (msg->getCodeNumber() == EN_LOGIN)
				{
					if (*user)
					{
						okay = true;
						str += MESSAGE(PGM_SCC_LOGIN);
						unique_lock<mutex> lck(Manager::mtx_roomMap);
						if (Manager::_roomMap.size() > 0)
						{
							lck.unlock();
							str += Manager::room_list();
							str += MESSAGE_END();
						}
						else
						{
							lck.unlock();
							str += MSG_ARG_DELIM;
						}
						unique_lock<mutex> lck2(mtx_vu);
						vu.insert(*user);
						lck2.unlock();
						shouldBreak = true;
					}
					else
					{
						str += MESSAGE(PGM_ERR_LOGIN);
						str += MSG_ARG_DELIM;
					}
				}
				else //Registering
				{
					if (vs[0].length() > 20 || vs[1].length() > 20)
					{
						str += MESSAGE(PGM_ERR_INFO_TOO_LONG);
						str += MSG_ARG_DELIM;
					}
					else
					{
						okay = Manager::register_user(**user);
						str += MESSAGE(PGM_SCC_REGISTER);
						unique_lock<mutex> lck(Manager::mtx_roomMap);
						if (Manager::_roomMap.size() > 0)
						{
							lck.unlock();
							str += Manager::room_list();
							str += MESSAGE_END();
						}
						else
						{
							lck.unlock();
							str += MSG_ARG_DELIM;
						}

						unique_lock<mutex> lck2(mtx_vu);
						vu.insert(*user);
						lck2.unlock();
						shouldBreak = true;
					}
				}

				this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
				if(shouldBreak)
				{
					break;
				}
			}
		}

		delete msg;
	}

	delete buf;
	return true;
}

void Server::HandleClient(SOCKET client)
{
	User* user = NULL;
	char buf[MY_MAX_LEN + 1] = { 0 };
	Message* msg;
	if (!LoginClient(&user, client))
	{
		return;
	}

	while (true) //only exiting of the loop in log-out case
	{
		while (!user->getRoom()) //while the user is in the lobby, and does not won't to log out
		{
			int err = this->Receive(client, (char*)buf, MY_MAX_LEN);
			if (err == -1)
			{
				unique_lock<mutex> lck(mtx_vu);
				set<User*>::iterator it = vu.find(user);
				if (it != vu.end())
					vu.erase(it); //Erasing the user from connected list
				lck.unlock();
				return;
			}
			else if(err == 0)
			{
				continue;
			}
			cout << user->getUsername() << ": " << string(buf) << endl;
			msg = new Message(string(buf));
			if (!msg->check_validity())
			{
				string str = ERROR_MSG();
				str += MSG_ARG_DELIM;
				this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
			}
			else
			{
				int code_num = msg->getCodeNumber();
				string str;
				vector<string> vs = msg->getMsgStrings();

				switch (code_num)
				{
				case EN_LOGOUT:
					return;
					break;
				case RM_ROOM_LIST:
					str += MESSAGE(PGM_CTR_ROOM_LIST);
					str += Manager::room_list();
					str += MSG_ARG_DELIM;
					if(str[str.length() - 2] != MSG_ARG_DELIM)
						str += MSG_ARG_DELIM;
					break;
				case RM_CREATE_GAME:
					if (vs.size() != 1)
					{
						str += ERROR_MSG();
					}
					else
					{
						Manager::add_room(*user, vs[0]);
						unique_lock<mutex> lck(Manager::mtx_roomMap);
						Room* r = Manager::_roomMap.find(user->getUsername())->second;
						r->add_user(*user);
						r->setAdmin(user);
						lck.unlock();
						user->setIsAdmin(true);
						user->setRoom(r);
						str += MESSAGE(PGM_SCC_GAME_CREATED);
						str += MSG_ARG_DELIM;
						this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
					}
					break;
				case RM_JOIN_GAME:
					if (vs.size() != 1)
					{
						str += ERROR_MSG();
					}
					else
					{
						unique_lock<mutex> lck(Manager::mtx_roomMap);
						if (Manager::_roomMap.find(vs[0]) != Manager::_roomMap.end())
						{
							if(!(Manager::_roomMap.find(vs[0])->second->is_open()))
							{
								str = MESSAGE(PGM_ERR_IN_GAME);
								str += MSG_ARG_DELIM;
								this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
								continue;
							}
							
							lck.unlock();
							if (AddUserToRoom(*user, vs[0]))
							{
								str += MESSAGE(PGM_SCC_GAME_JOIN);
								str += Manager::user_list(vs[0]);
								str += MESSAGE_END();
								this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
							}
							else
							{
								str += MESSAGE(PGM_ERR_ROOM_FULL);
								str += vs[0];
								str += MESSAGE_END();
							}
						}
						else
						{
							lck.unlock();
							str += MESSAGE(PGM_ERR_ROOM_NOT_FOUND);
							str += vs[0];
							str += MESSAGE_END();
						}
					}
					break;
				default:
					str += MESSAGE(PGM_MER_ACCESS);
					str += MSG_ARG_DELIM;
					break;
				}

				if (!user->getRoom())
				{
					this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
				}
			}
		}

		while (user->getRoom()) //While the user is in a room
		{
			if(!user->getRoom()->is_open()) //In game
			{
				while(user->getRoom() && !user->getRoom()->is_open())
				{
					int err = this->Receive(client, (char*)buf, MY_MAX_LEN);
					if (err == -1)
					{
						unique_lock<mutex> lck(mtx_vu);
						set<User*>::iterator it = vu.find(user);
						if (it != vu.end())
							vu.erase(it); //Erasing the user from connected list
						lck.unlock();
						return;
					}
					else if(err == 0)
					{
						continue;
					}
					msg = new Message(buf);
					if (!msg->check_validity())
					{
						string str = ERROR_MSG();
						this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
					}
					else
					{
						int code_num = msg->getCodeNumber();
						string str, msg_to_users;
						switch(code_num)
						{
						case RM_CLOSE_GAME: //Admin wanting to close the game
							if (user->isAdmin())
							{
								str += MESSAGE(PGM_SCC_GAME_CLOSE);
								str += MSG_ARG_DELIM;
								user->setIsAdmin(false);
								msg_to_users += MESSAGE(PGM_CTR_ROOM_CLOSED);
								msg_to_users += MSG_ARG_DELIM;
								unique_lock<mutex> lck(Manager::mtx_roomMap);
								Room* r = Manager::_roomMap.find(user->getUsername())->second;
								lck.unlock();
								r->announce((void*)msg_to_users.c_str(), user->getUsername());
								Manager::remove_room(r);
							}
							else
							{
								str += MESSAGE(PGM_MER_ACCESS);
								str += MSG_ARG_DELIM;
							}
							break;
						case RM_LEAVE_GAME: //A regular player wants to quit
							if (user->isAdmin())
							{
								str += MESSAGE(PGM_SCC_GAME_CLOSE);
								str += MSG_ARG_DELIM;
								user->setIsAdmin(false);
								msg_to_users += MESSAGE(PGM_CTR_ROOM_CLOSED);
								msg_to_users += MSG_ARG_DELIM;
								unique_lock<mutex> lck(Manager::mtx_roomMap);
								Room* r = Manager::_roomMap.find(user->getUsername())->second;
								lck.unlock();
								r->announce((void*)msg_to_users.c_str(), user->getUsername());
								Manager::remove_room(r);
							}
							else
							{
								str += MESSAGE(PGM_SCC_GAME_CLOSE);
								str += MSG_ARG_DELIM;
								msg_to_users += MESSAGE(PGM_CTR_REMOVE_USER);
								msg_to_users += user->getUsername();
								msg_to_users += MESSAGE_END();
								user->getRoom()->announce((void*)msg_to_users.c_str(), user->getUsername());
								user->getRoom()->delete_user(*user);
								user->setRoom(NULL);
							}
							break;

						case GM_PLAY:
							{
								user->getRoom()->play_turn(msg->getMsgStrings()[0], user);
								continue;
							}
							break;
						case GM_DRAW:
							{
							vector<string> params = msg->getMsgStrings();
							Room* room = user->getRoom();

							if(params.size() == 1)
							{
								if(user == room->getPlayers()[room->getCurrentPlayer()])
								{
									if(room->draw_cards(std::stoi(params[0])))
									{
										vector<Card> cards;
										int num_of_cards = std::stoi(params[0]);
										string card_list = "";
										for(int i = 0 ; i < num_of_cards ; i++)
										{
											Card c = Card();
											cards.push_back(c);
											card_list += c.ToString() + ",";
										}

										card_list.pop_back();
										user->add_cards(cards);

										str = MESSAGE(GAM_SCC_DRAW) + card_list + MSG_ARG_DELIM + NEXT_PLAYER(room)->getUsername() + MESSAGE_END();
										msg_to_users = MESSAGE(GAM_CTR_DRAW_CARDS) + params[0] + MSG_ARG_DELIM + NEXT_PLAYER(room)->getUsername() + MESSAGE_END();
										room->announce((void*)msg_to_users.c_str(), user->getUsername());
										room->switchPlayer();
									}
									else
									{
										str = MESSAGE(GAM_ERROR_WRONG_DRAW) + MSG_ARG_DELIM;
									}
								}
								else
								{
									str = MESSAGE(GAM_ERR_NOT_YOUR_TURN) + MSG_ARG_DELIM;
								}
							}
							else
							{
								str = MESSAGE(PGM_MER_MESSAGE) + MSG_ARG_DELIM;
							}
							}
							break;
						}

						this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
					}
				}
			}
			else //Waiting in room
			{
				int err = this->Receive(client, (char*)buf, MY_MAX_LEN);
				if (err == -1)
				{
					unique_lock<mutex> lck(mtx_vu);
					set<User*>::iterator it = vu.find(user);
					if (it != vu.end())
						vu.erase(it); //Erasing the user from connected list
					lck.unlock();
					return;
				}
				else if(err == 0)
				{
					continue;
				}
				msg = new Message(buf);
				if (!msg->check_validity())
				{
					string str = ERROR_MSG();
					this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
				}
				else
				{
					int code_num = msg->getCodeNumber();
					string str, msg_to_users;

					switch (code_num)
					{
					case RM_START_GAME: //Admin's wanting to start the game
						if (user->isAdmin())
						{
							if(!(Manager::_roomMap.find(user->getUsername())->second->start_game()))
							{
								str = MESSAGE(PGM_ERR_TOO_FEW_USERS);
								str += MSG_ARG_DELIM;
							}
							else
							{
								continue;
							}
						}
						else
						{
							str += MESSAGE(PGM_MER_ACCESS);
							str += MSG_ARG_DELIM;
						}
						break;
					case RM_LEAVE_GAME: //A regular player wants to quit
						if (user->isAdmin())
						{
							str += MESSAGE(PGM_SCC_GAME_CLOSE);
							str += MSG_ARG_DELIM;
							user->setIsAdmin(false);
							msg_to_users += MESSAGE(PGM_CTR_ROOM_CLOSED);
							msg_to_users += MSG_ARG_DELIM;
							unique_lock<mutex> lck(Manager::mtx_roomMap);
							Room* r = Manager::_roomMap.find(user->getUsername())->second;
							lck.unlock();
							r->announce((void*)msg_to_users.c_str(), user->getUsername());
							Manager::remove_room(r);
						}
						else
						{
							str += MESSAGE(PGM_SCC_GAME_CLOSE);
							str += MSG_ARG_DELIM;
							msg_to_users += MESSAGE(PGM_CTR_REMOVE_USER);
							msg_to_users += user->getUsername();
							msg_to_users += MESSAGE_END();
							user->getRoom()->announce((void*)msg_to_users.c_str(), user->getUsername());
							user->getRoom()->delete_user(*user);
							user->setRoom(NULL);
						}
						break;
					case RM_CLOSE_GAME: //Admin wanting to close the game
						if (user->isAdmin())
						{
							str += MESSAGE(PGM_SCC_GAME_CLOSE);
							str += MSG_ARG_DELIM;
							user->setIsAdmin(false);
							msg_to_users += MESSAGE(PGM_CTR_ROOM_CLOSED);
							msg_to_users += MSG_ARG_DELIM;
							unique_lock<mutex> lck(Manager::mtx_roomMap);
							Room* r = Manager::_roomMap.find(user->getUsername())->second;
							lck.unlock();
							r->announce((void*)msg_to_users.c_str(), user->getUsername());
							Manager::remove_room(r);
						}
						else
						{
							str += MESSAGE(PGM_MER_ACCESS);
							str += MSG_ARG_DELIM;
						}
						break;
					default:
						str += MESSAGE(PGM_MER_ACCESS);
						str += MSG_ARG_DELIM;
						break;
					}

					this->Send(client, (void*)str.c_str(), MY_MAX_LEN);
				}
			}
		}
	}

	unique_lock<mutex> lck(mtx_vu);
	set<User*>::iterator it = vu.find(user);
	if (it != vu.end())
		vu.erase(it); //Erasing the user from connected list
	lck.unlock();
}

bool Server::AddUserToRoom(User& user, string admin_name)
{
	unique_lock<mutex> lck(Manager::mtx_roomMap);
	map<string, Room*>::iterator it = Manager::_roomMap.find(admin_name);

	if (it != Manager::_roomMap.end())
	{
		lck.unlock();
		if (it->second->add_user(user))
		{
			vector<User*> players = it->second->getPlayers();
			string str;
			str += MESSAGE(PGM_CTR_NEW_USER);
			str += user.getUsername();
			str += MESSAGE_END();
			it->second->announce((void*)str.c_str(), user.getUsername());
			user.setRoom(it->second); //Setting the room the user is in, as the current room

			return true;
		}
	}
	else
	{
		lck.unlock();
	}

	return false;
}

int Server::Send(SOCKET ClientSocket, void* buf, int max_len, int flags)
{
	return send(ClientSocket, (const char*)buf, max_len, flags);
}

int Server::Receive(SOCKET ClientSocket, void* buf, int max_len, int flags)
{
	int num = -1;
	bool okay = true;
	char* temp = new char[max_len];
	SOCKET arr[1] = {ClientSocket};
	struct fd_set f;
	struct timeval tv;

	FD_ZERO(&f);
	FD_SET(ClientSocket, &f);

	tv.tv_usec = USEC_TO_WAIT;
	
	if(select(0, &f, NULL, NULL, &tv) > 0) //or maybe first parameter ClientSocket+1?
	{
		if (recv(ClientSocket, temp, max_len, 0) < 0)
		{
			delete temp;
			return -1;
		}

		while (okay)
		{
			do
			{
				if (num >= max_len - 1)
				{
					delete temp;
					return num;
				}
				num++;
				((char*)buf)[num] = temp[num];
			} while (((char*)buf)[num] != MSG_ARG_DELIM);

			if (num >= max_len - 1)
			{
				delete temp;
				return num;
			}
			num++;
			((char*)buf)[num] = temp[num];

			if (((char*)buf)[num] == MSG_ARG_DELIM)
			{
				okay = false;
			}
		}

		delete temp;
		return num;
	}
	
	delete temp;
	return 0;
}

Server::~Server()
{
	for (std::vector<std::thread*>::iterator it = vt.begin(); it != vt.end(); it++)
	{
		(*it)->join();
		free(*it);
	}
}