#pragma once
#include <iostream>
using namespace std;
class Card
{
public:
	Card(); //Default constructor - results in a random card
	Card(string str); //String to card e.g 'r+'
	Card(char type, char color); //Two chars to car e.g 'r', '+'
	bool operator==(const Card& other) const;
	string ToString() const; //Returning the string representing the card
	char getType() const;
	char getColor() const;
	~Card(void);
private:
	char _type; //The type of the card. e.g '1', '+'...
	char _color; //The color of the card. e.g 'r', 'y'...
};

