#include "Message.h"
#include <string>
#include "TakiProtocol.h"

Message::Message(string msg) : _message(msg)
{
}

bool Message::check_validity()
{
	int len = _message.find("||") + 2;
	if(len == 1)
		return false;
	if(_message[0] == MSG_START && _message[1] >= '1' && _message[1] <= '9')
	{
		int i = 1;
		while(_message[i] >= '0' && _message[i] <= '9')
		{
			i++;
		}

		if(_message[i] == MSG_ARG_DELIM)
		{
			return true;
		}
	}

	return false;
}

int Message::getCodeNumber()
{
	return stoi(_message.substr(1, _message.find_first_of('|') - 1));
}

vector<string> Message::getMsgStrings()
{
	vector<string> v;
	for(int i = 0 ; _message[i] ; i++)
	{
		if(_message[i] == MSG_ARG_DELIM)
			if(_message[i + 1] == MSG_ARG_DELIM)
				break;
			else
				v.push_back(_message.substr(i + 1, _message.find(MSG_ARG_DELIM, i + 1) - (i + 1)));
	}

	return v;
}

Message::~Message()
{
}
