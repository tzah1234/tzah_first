#include "stdafx.h"

string encrypt(std::string msg)
{
    string tmp(ENC_KEY);

    while(tmp.size() < msg.size())
        tmp += tmp;
    
    for(unsigned int i = 0 ; i < msg.size() ; i++)
        msg[i] ^= tmp[i];

    return msg;
}

string decrypt(std::string msg)
{
	return encrypt(msg);
}