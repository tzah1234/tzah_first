#include "User.h"
#include <string>

User::User(string username, string password, SOCKET user_socket) : _username(username), _password(password), _userSocket(user_socket), _isAdmin(false), _room(NULL), _deck(NULL)
{
}

bool User::operator<(const User& other)
{
	return (_username < other.getUsername());
}

bool User::operator==(const User& other)
{
	return (_username == other.getUsername());
}

bool User::play_turn(const vector<Card>& cards)
{
	DECK new_deck(*this->_deck);

	for(unsigned int i = 0 ; i < cards.size() ; i++)
	{
		vector<Card>::iterator pos = std::find(new_deck.begin(), new_deck.end(), cards[i]);
		
		if(pos != new_deck.end())
		{
			new_deck.erase(pos);
		}
		else
		{
			return false;
		}
	}

	(*(this->_deck)) = new_deck;

	return true;
}

string User::getUsername() const
{
	return _username;
}

string User::getPassword() const
{
	return _password;
}

SOCKET User::getSocket() const
{
	return _userSocket;
}

bool User::isAdmin() const
{
	return _isAdmin;
}

Room* User::getRoom() const
{
	return _room;
}

void User::setRoom(Room* room)
{
	this->_room = room;
}

void User::setIsAdmin(bool isAdmin)
{
	this->_isAdmin = isAdmin;
}

void User::setDeck(DECK* deck)
{
	this->_deck = deck;
}

void User::add_cards(const vector<Card>& cards)
{
	for(unsigned int i = 0 ; i < cards.size() ; i++)
	{
		this->_deck->push_back(cards[i]);
	}
}

User::~User()
{
}