#include "Room.h"
#include "Manager.h"
#include <string>
#include "Server.h"
#include "TakiProtocol.h"

Room::Room(string name, User* admin) : _roomName(name), _admin(admin), _players(), _inGame(false), _currentPlayer(0), _turnModifier(0), _drawCounter(1), _lastCard(), _num_of_turns(0)
{
}

Room::Room(const Room& room)
{
}

bool Room::add_user(User &user)
{
	unique_lock<mutex> lck(mtx_players);
	
	if(_players.size() < 4)
	{
		_players.push_back(&user);
		lck.unlock();
		return true;
	}
	lck.unlock();
	return false;
}

void Room::delete_user(User &user)
{
	unique_lock<mutex> lck(mtx_players);
	
	auto it = find(_players.begin(), _players.end(), &user);
	
	if(it != _players.end())
	{
		auto index = distance(_players.begin(), it);
		_players.erase(_players.begin() + index);
	}

	lck.unlock();
}

vector<User*> Room::getPlayers() const
{
	return this->_players;
}

bool Room::is_open()
{
	return !(this->_inGame);
}

void Room::close()
{
	_players.clear();
	_admin = NULL;
	_inGame = false;

	//Manager::remove_room(this); //will it work?
}

void Room::announce(void* buf, string sender)
{
	unsigned int i;
	for(i = 0 ; i < _players.size() ; i++)
	{
		if(_players[i]->getUsername() != sender)
		{
			Server::Send(_players[i]->getSocket(), buf, MY_MAX_LEN);
		}
	}
}

bool Room::is_in_room(User const &user)
{
	for(int i = 0 ; i < 0 ; i++)
	{
		if(*_players[i] == user)
			return true;
	}

	return false;
}

bool Room::start_game()
{
	if(_players.size() < 2)
	{
		return false;
	}

	Manager::start_game_db(*this);

	_inGame = true;
	_lastCard = Card();
	_turnModifier = 0;
	_drawCounter = 1;
	_currentPlayer = 0;

	vector<DECK*>* decks = this->shuffle_cards_start_game(_players.size());
	for(unsigned int i = 0 ; i < _players.size() ; i++)
	{
		string msg = MESSAGE(PGM_CTR_GAME_STARTED);
		msg += std::to_string(_players.size()); //Amount of players
		msg += MSG_ARG_DELIM;
		unsigned int j;

		for(j = 0 ; j < (*decks)[i]->size() - 1 ; j++)
		{
			msg += (*((*decks)[i]))[j].ToString();
			msg += ',';
		}
		msg += (*((*decks)[i]))[j].ToString();
		msg += MSG_ARG_DELIM;
		msg += _lastCard.ToString();
		msg += MESSAGE_END();
		Server::Send(_players[i]->getSocket(), (void*)msg.c_str(), MY_MAX_LEN);
		_players[i]->setDeck((*decks)[i]);
	}
	
	return true;
}

bool Room::play_turn(const string& cards, User* user)
{
	bool isOk = true;
	vector<Card> moves;
	string cards_list(cards);
	//Split the string to cards
	int pos = 0;
	while( (pos = cards_list.find(",")) != std::string::npos)
	{
		try
		{
			moves.push_back(Card(cards_list.substr(0, pos)));
		}
		catch(...)
		{
			isOk = false;
			break;
		}

		if(isOk)
		{
			cards_list.erase(0, pos + 1);
		}
	}

	try
	{
		moves.push_back(Card(cards_list));
	}
	catch(...)
	{
		isOk = false;
	}

	if(!isOk)
	{
		string str = MESSAGE(GAM_ERR_ILLEGAL_CARD) + cards_list.substr(0, pos) + MESSAGE_END();
		Server::Send(user->getSocket(), (void*)str.c_str(), MY_MAX_LEN);
	}
	else
	{
		return this->play_turn(moves, user);
	}

	return false;
}

bool Room::play_turn(const vector<Card>& moves, User* user)
{
	string res;

	if(this->_players[this->_currentPlayer] == user)
	{
		res = this->is_turn_legal(moves);

		if(res == "")
		{
			if(user->play_turn(moves))
			{
				string msg = MESSAGE(GAM_CTR_TURN_COMPELTE);
				unsigned int i;
				for(i = 0 ; i < moves.size() - 1 ; i++)
				{
					msg += moves[i].ToString();
					msg += ',';
				}
				if(!moves.empty())
					msg += moves[i].ToString();

				_lastCard = moves[i];

				int next_player = (this->_currentPlayer + 1) % this->_players.size();
				msg += MSG_ARG_DELIM + _players[next_player]->getUsername() + MESSAGE_END();
				string msg_to_player = MESSAGE(GAM_SCC_TURN) + _players[next_player]->getUsername() + MESSAGE_END();

				this->_num_of_turns++;
				this->_currentPlayer = next_player;
				this->announce((void*)msg.c_str(), user->getUsername());
				Server::Send(user->getSocket(), (void*)msg_to_player.c_str(), MY_MAX_LEN);
				//handle the last card. +2?
				return true;
			}
			
			res = MESSAGE(GAM_ERR_NO_SUCH_CARD) + MSG_ARG_DELIM;

			Server::Send(user->getSocket(), (void*)res.c_str(), MY_MAX_LEN);

			return false;
		}

		Server::Send(user->getSocket(), (void*)res.c_str(), MY_MAX_LEN);

		return false;
	}

	res = MESSAGE(GAM_ERR_NOT_YOUR_TURN) + MSG_ARG_DELIM;

	Server::Send(user->getSocket(), (void*)res.c_str(), MY_MAX_LEN);

	return false;
}

bool Room::draw_cards(int card_number)
{
	if(this->is_draw_legal(card_number))
	{
		this->_drawCounter = 1;
		
		return true;
	}

	return false;
}

string Room::getName()
{
	return _roomName;
}

void Room::setLastCard(Card cr)
{
	_lastCard = cr;
}

Card Room::getLastCard()
{
	return _lastCard;
}

string Room::is_taki_legal(const vector<Card>& moves)
{
	char color = moves[0].getColor();
	for(unsigned int i = 1 ; i < moves.size() - 1 ; i++)
	{
		if(moves[i].getColor() != color)
		{
			if(moves[i - 1].getType() == TAKI_CARD_PLUS)
			{
				if(moves[i].getType() == TAKI_CARD_PLUS)
					continue;
			}

			return MESSAGE(GAM_ERR_ILLEGAL_CARD) + moves[i].ToString() + MESSAGE_END();
		}
	}

	if(moves[moves.size() - 1].getColor() != color)
	{
		if(moves[moves.size() - 1].getColor() != moves[moves.size() - 2].getColor() && 
			moves[moves.size() - 1].getType() != TAKI_CARD_CHANGECOLOR &&
			moves[moves.size() - 1].getType() != TAKI_CARD_SUPERTAKI)
			return MESSAGE(GAM_ERR_ILLEGAL_CARD) + moves[moves.size() - 1].ToString() + MESSAGE_END();
	}

	return "";
}

string Room::is_turn_legal(const vector<Card>& moves)
{
	if(moves.size() == 0)
	{
		return MESSAGE(GAM_ERR_ILLEGAL_CARD) + std::to_string(MSG_ARG_DELIM);
	}

	if(moves[0].getColor() != this->_lastCard.getColor())
	{
		if(moves[0].getType() != this->_lastCard.getType())
		{
			if(moves[0].getType() != TAKI_CARD_SUPERTAKI && moves[0].getType() != TAKI_CARD_CHANGECOLOR)
				return MESSAGE(GAM_ERR_ILLEGAL_CARD) + moves[0].ToString() + MESSAGE_END();
		}
	}

	if(this->_drawCounter > 1 && !(moves.size() == 1 && moves[0].getType() != TAKI_CARD_TAKETWO))
		return MESSAGE(GAM_ERR_DRAW_OR_TAKETWO) + std::to_string(MSG_ARG_DELIM);

	if(moves[moves.size() - 1].getType() == TAKI_CARD_PLUS)
		return MESSAGE(GAM_ERR_LAST_CARD) + std::to_string(MSG_ARG_DELIM);
	
	if(moves.size() > 1)
	{
		if(moves[0].getType() == TAKI_CARD_TAKI || moves[0].getType() == TAKI_CARD_SUPERTAKI)
		{
			return is_taki_legal(moves);
		}
		else
		{
			if(moves[0].getType() != TAKI_CARD_PLUS)
			{
				return MESSAGE(GAM_ERR_ILLEGAL_ORDER) + moves[1].ToString() + MESSAGE_END();
			}

			char color = moves[0].getColor();
			unsigned int i;

			for(i = 1 ; i < moves.size() - 1 ; i++)
			{
				if(moves[i].getType() == TAKI_CARD_SUPERTAKI || moves[i].getType() == TAKI_CARD_TAKI)
				{
					if(moves[i - 1].getColor() != moves[i].getColor() && moves[i].getType() != TAKI_CARD_SUPERTAKI)
						return MESSAGE(GAM_ERR_ILLEGAL_CARD) + moves[i].ToString() + MESSAGE_END();

					vector<Card> from_taki(std::find(moves.begin(), moves.end(), moves[i]), moves.end());

					return is_taki_legal(from_taki);
				}

				if(moves[i].getType() != TAKI_CARD_PLUS)
					return MESSAGE(GAM_ERR_ILLEGAL_CARD) + moves[i].ToString() + MESSAGE_END();
			}
			
			if(moves[i].getColor() != moves[i - 1].getColor() && moves[i].getType() != TAKI_CARD_CHANGECOLOR)
			{
				return MESSAGE(GAM_ERR_ILLEGAL_CARD) + moves[i].ToString() + MESSAGE_END();
			}
		}
	}
	
	return "";
}

bool Room::is_draw_legal(int num_of_cards)
{
	return (this->_drawCounter == num_of_cards);
}

DECK* Room::shuffle_cards(int num_of_cards)
{
	DECK* user_deck = new DECK();
	for(int i = 0 ; i < num_of_cards ; i++)
	{
		user_deck->push_back(Card());
	}

	return user_deck;
}


vector<DECK*>* Room::shuffle_cards_start_game(int num_of_players)
{
	vector<DECK*>* decks = new vector<DECK*>;
	for(int i = 0 ; i < num_of_players ; i++)
	{
		DECK* user_deck = this->shuffle_cards(NUM_OF_CARDS_IN_FIRST_HAND);

		decks->push_back(user_deck);
	}

	return decks;
}

User* Room::getAdmin() const
{
	return _admin;
}

void Room::setAdmin(User* admin)
{
	_admin = admin;
}

void Room::setDbId(int db_id)
{
	this->_db_id = db_id;
}

int Room::getDbId() const
{
	return this->_db_id;
}

int Room::getNumOfTurns() const
{
	return this->_num_of_turns;
}

int Room::getCurrentPlayer() const
{
	return this->_currentPlayer;
}

void Room::switchPlayer()
{
	this->_currentPlayer++;
	this->_currentPlayer %= this->_players.size();
}

Room::~Room()
{
	this->close();
}