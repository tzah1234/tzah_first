#pragma once
#include <iostream>
#include <WS2tcpip.h>
#include <vector>
#include "Card.h"

#define DECK vector<Card>

class Room;
using namespace std;

class User
{
public:
	User(string username, string password, SOCKET user_socket); //C'tor
	bool User::operator<(const User& other);
	bool operator==(const User& other);
	bool play_turn(const vector<Card>& cards);
	string getUsername() const;
	string getPassword() const;
	SOCKET getSocket() const;
	bool isAdmin() const;
	Room* getRoom() const;
	void setIsAdmin(bool isAdmin);
	void setRoom(Room* room);
	void setDeck(DECK* deck);
	void add_cards(const vector<Card>& cards);
	~User();
private:
	string _username;
	string _password;
	Room* _room;
	bool _isAdmin;
	DECK* _deck;
	SOCKET _userSocket;
};

