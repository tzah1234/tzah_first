#include "Manager.h"
#include <string>
#include "TakiProtocol.h"
#include <time.h>
#include "stdafx.h"

#define ROOM_EXISTS(a) (Manager::_roomMap.find(a) != Manager::_roomMap.end())
#define DB_NAME "taki_db"

static bool currUserExists = false; 

map<string, Room*> Manager::_roomMap; //Admin's Username, Room
mutex Manager::mtx_roomMap;
sqlite3* Manager::_db;

bool Manager::init_db()
{
	int err = sqlite3_open(DB_NAME, &(Manager::_db));
	if(err)
	{
		return false;
	}

	return true;
}

bool Manager::register_user(User const &user)
{
	if(!Manager::is_exist(user, true))
	{		
		string str = "insert into users (username, password_hash) values ('" + user.getUsername() + "', '" + encrypt(user.getPassword()) + "');";

		int err = sqlite3_exec(Manager::_db, str.c_str(), NULL, 0, NULL);
		if(err)
		{
			cout << "Error registering " << user.getUsername() << ". Error: " << sqlite3_errmsg(Manager::_db) << endl;
			return false;
		}
	}
	else
		return false;
	
	return true;
}

void Manager::login_user(User const &user)
{
	/*TODO DATABASE ADDITION*/
}

bool Manager::is_exist(User const &user, bool ignore_pass) 
{
	string str = "select * from users where username = '" + user.getUsername();

	if(ignore_pass)
	{
		str += "';";
	}
	else
	{
		str += "' and password_hash = '" + decrypt(user.getPassword()) + "';";
	}

	int err = sqlite3_exec(Manager::_db, str.c_str(), 
	[](void*, int, char**, char**)
	{
		currUserExists = true;
		return 0;
	}, 0, NULL);

	if(err)
	{
		cout << "Error checking if the user exists." << endl;
	}

	if(currUserExists)
	{
		currUserExists = false;
		return true;
	}

	currUserExists = false;

	return false;
}

User* Manager::try_login(string user_name, string user_password)
{
	//string str = "select";
//	int err = sqlite3_exec(Manager::_db, str.c_str(), [](){}, 0, NULL);

	return NULL;
}

string Manager::room_list() //ROOMS DIFFERENTIATED BY | AND ,
{
	string str;
	map<string, Room*>::iterator it;
	unique_lock<mutex> lck(mtx_roomMap);

	if(Manager::_roomMap.size() > 0)
	{
		for(it = Manager::_roomMap.begin() ; it != Manager::_roomMap.end() ; it++)
		{
			str += it->second->getName(); //Room's Name
			str += ',';
			str += it->first; //Admin's Name
			str += ',';
			str += std::to_string(it->second->getPlayers().size()); //Number of players
			str += MSG_ARG_DELIM;
		}
		str = str.substr(0, str.size()-1);
	}

	lck.unlock();
	return str;
}

string Manager::user_list(string room_name)
{
	string str;

	unique_lock<mutex> lck(mtx_roomMap);
	vector<User*> players = Manager::_roomMap.find(room_name)->second->getPlayers();
	lck.unlock();

	unsigned int i;
	for(i = 0 ; i < players.size() - 1 ; i++)
	{
		str += players[i]->getUsername();
		str += ',';
	}
	str += players[i]->getUsername();

	return str;
}

void Manager::add_room(User const &admin, string room_name)
{
	string user = admin.getUsername();

	unique_lock<mutex> lck(mtx_roomMap);
	Manager::_roomMap.insert(pair<string, Room*>(user,	new Room(room_name, (User*)&admin)));
	lck.unlock();
}

void Manager::remove_room(Room *room)
{
	unique_lock<mutex> lck(mtx_roomMap);
	vector<User*> players = room->getPlayers();

	for(unsigned int i = 0 ; i < players.size(); i ++)
	{
		players[i]->setRoom(NULL);
	}

	if (Manager::_roomMap.find(room->getAdmin()->getUsername()) != _roomMap.end())
	{
		Manager::_roomMap.erase(room->getAdmin()->getUsername());
		delete room;
	}
	lck.unlock();
}

bool Manager::start_game_db(Room& room)
{
	string str = "insert into games(game_start) values(";
	str += std::to_string(time(0));
	str += ");";

	int err = sqlite3_exec(Manager::_db, str.c_str(), NULL, 0, NULL);
	if(err)
	{
		return false;
	}

	room.setDbId((int)sqlite3_last_insert_rowid(Manager::_db));

	return true;
}

bool Manager::end_game_db(const Room& room)
{
	string str = "update games set game_end = " + std::to_string(time(0)) + ", turns_number = " + std::to_string(room.getNumOfTurns()) + " where game_id = " + std::to_string(room.getDbId()) + " (";

	int err = sqlite3_exec(Manager::_db, str.c_str(), NULL, 0, NULL);
	if(err)
	{
		return false;
	}

	return true;
}

