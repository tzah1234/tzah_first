#pragma once
#include "Room.h"
using namespace std;
#include <map>
#include <vector>
#include "User.h"
#include "sqlite3.h"

class Manager
{
public:
	static bool init_db();
	static bool register_user(User const &user); //Register a user
	static void login_user(User const &user); //Login a user
	static bool is_exist(User const &user, bool ignore_pass); //Check if a certain user exists
	static User* try_login(string user_name, string user_password); //Try login a user, returning the info
	static string room_list(); //Returning a room list, separated by ','
	static string user_list(string room_name); //Returning a user list in a room, separated by ','
	static void add_room(User const &admin, string room_name); //Adding a room
	static void remove_room(Room *room); //Removing a room
	static bool start_game_db(Room &room); //Start a game in the database
	static bool end_game_db(const Room &room); //End game in the database

	static map<string, Room*> _roomMap;
	static mutex mtx_roomMap;
	static sqlite3* _db;
};

