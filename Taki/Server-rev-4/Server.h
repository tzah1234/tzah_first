#pragma once
#define MY_MAX_LEN 1024
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <time.h>
#include "User.h"

#define MESSAGE(a) MSG_START + std::to_string(a) + MSG_ARG_DELIM
#define MESSAGE_END() string(1, MSG_ARG_DELIM) + string(1, MSG_ARG_DELIM)
#define ERROR_MSG() MESSAGE(PGM_MER_MESSAGE) + MSG_ARG_DELIM

using namespace std;
class Server
{
public:
	Server(string Port); //C'tor for the server, starting its run
	Server(const Server&);
	void foo();
	void AcceptConnections(SOCKET client);	//Accepting connections, handling clients from there on
	bool LoginClient(User** user, SOCKET client); //Have the client login or register, either way - identified
	void HandleClient(SOCKET client); //Handle the client's overall visit
	bool AddUserToRoom(User& user, string admin_name); //Add a user to a certain room
	static int Send(SOCKET ClientSocket, void* buf, int max_len, int flags = 0); //Simplified send()
	static int Receive(SOCKET ClientSocket, void* buf, int max_len, int flags = 0); //Simplified recv()
	~Server(); //D'tor
private:
	SOCKET _socket; //Listening Socket
	struct addrinfo* _details;
	WSADATA _wsaData;
};