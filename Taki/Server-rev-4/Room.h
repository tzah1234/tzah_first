#pragma once
#include "User.h"
#include <iostream>
#include <mutex>

using namespace std;

class Room
{
public:
	Room(string, User*); //C'tor
	Room(const Room&);
	string getName(); //Get room name
	bool add_user(User &user); //Add a user to the room
	void delete_user(User &user); //Delete a user
	bool is_open(); //Check if the room is open
	void close(); //Close the room
	void announce(void* buf, string sender); //Announce something to players
	vector<User*> getPlayers() const; //Get the players
	bool is_in_room(User const &user); //Check if the user is in the room
	bool start_game(); //Start a game
	bool play_turn(const vector<Card>& moves, User* user);
	bool play_turn(const string& moves, User* user);
	bool draw_cards(int card_number);
	string is_taki_legal(const vector<Card>& moves);
	string is_turn_legal(const vector<Card>& moves);
	bool is_draw_legal(int num_of_cards);
	Card getLastCard();
	void setLastCard(Card cr);
	DECK* shuffle_cards(int num_of_cards);
	vector<DECK*>* shuffle_cards_start_game(int num_of_players);
	User* getAdmin() const; //Get the admin
	void setAdmin(User*);
	int getDbId() const;
	void setDbId(int);
	int getNumOfTurns() const;
	int getCurrentPlayer() const;
	void switchPlayer();
	~Room();
private:
	string _roomName;
	User* _admin;
	mutex mtx_players;
	vector<User*> _players;
	int _db_id;
	bool _inGame;
	int _currentPlayer;
	int _num_of_turns;
	int _turnModifier;
	int _drawCounter;
	Card _lastCard;
};

