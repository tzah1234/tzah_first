#include "Card.h"
#include <string>
#include <time.h>

static string valid_colors = "rygb";
static string valid_types = "13456789+!<$%^*";

Card::Card()
{
	this->_color = valid_colors[rand() % valid_colors.length()];
	this->_type = valid_types[rand() % valid_types.length()];
}

Card::Card(string str)
{
	if(str.length() != 2)
		throw "Invalid Card";
	if( valid_types.find_first_of(str[0]) == string::npos || valid_colors.find_first_of(str[1]) == string::npos)
		throw "Invalid Card";
	this->_type = str[0];
	this->_color = str[1];
}

Card::Card(char type, char color) : _type(type), _color(color)
{
}

bool Card::operator==(const Card& other) const
{
	return (this->ToString() == other.ToString());
}

string Card::ToString() const
{
	string str;
	str += _type;
	str += _color;

	return str;
}

char Card::getColor() const
{
	return this->_color;
}

char Card::getType() const
{
	return this->_type;
}

Card::~Card(void)
{
}
