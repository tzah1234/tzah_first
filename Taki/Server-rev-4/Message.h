#pragma once
#include <iostream>
#include <WS2tcpip.h>
#include <vector>
using namespace std;
class Message
{
public:
	Message(string);
	bool check_validity(); //Check if the message is valid
	int getCodeNumber(); //Get the message's code number
	vector<string> getMsgStrings(); //Get all of the arguments
	~Message();
private:
	string _message; //The message string
};

