﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
//todo:: announce every time you exit a room
namespace TakiClient
{
    static class Global
    {
        public static Socket s;
        public static string username;
        public static string[] players;
        public static string input;

        public static Timer tm;

        public const int CHUNK_SIZE = 1024;

        public const char MSG_START = '@';
        public const char MSG_ARG_DELIM = '|';
        public const string MSG_END = "||";
        public const int NUM_OF_CARDS_IN_FIRST_HAND = 8;

        public const int EN_REGISTER = 1;
        public const int EN_LOGIN = 2;
        public const int EN_LOGOUT = 3;
        public const int RM_ROOM_LIST = 10;
        public const int RM_CREATE_GAME = 11;
        public const int RM_JOIN_GAME = 12;
        public const int RM_START_GAME = 13;
        public const int RM_LEAVE_GAME = 14;
        public const int RM_CLOSE_GAME = 15;
        public const int GM_PLAY = 20;
        public const int GM_DRAW = 21;
        public const int CH_SEND = 30;
        public const int PGM_SCC_LOGIN = 100;
        public const int PGM_SCC_REGISTER = 101;
        public const int PGM_SCC_GAME_CREATED = 102;
        public const int PGM_SCC_GAME_JOIN = 103;
        public const int PGM_SCC_GAME_CLOSE = 104;
        public const int PGM_CTR_NEW_USER = 110;
        public const int PGM_CTR_REMOVE_USER = 111;
        public const int PGM_CTR_GAME_STARTED = 112;
        public const int PGM_CTR_ROOM_CLOSED = 113;
        public const int PGM_CTR_ROOM_LIST = 114;
        public const int PGM_ERR_LOGIN = 120;
        public const int PGM_ERR_REGISTER_INFO = 121;
        public const int PGM_ERR_NAME_TAKEN = 122;
        public const int PGM_ERR_ROOM_FULL = 123;
        public const int PGM_ERR_ROOM_NOT_FOUND = 124;
        public const int PGM_ERR_TOO_FEW_USERS = 125;
        public const int PGM_ERR_INFO_TOO_LONG = 126;
        public const int PGM_ERR_IN_GAME = 127;
        public const int PGM_MER_MESSAGE = 130;
        public const int PGM_MER_ACCESS = 131;
        public const int GAM_SCC_TURN = 200;
        public const int GAM_SCC_DRAW = 201;
        public const int GAM_CTR_TURN_COMPELTE = 210;
        public const int GAM_CTR_DRAW_CARDS = 211;
        public const int GAM_CTR_GAME_ENDED = 212;
        public const int GAM_ERR_ILLEGAL_CARD = 220;
        public const int GAM_ERR_ILLEGAL_ORDER = 221;
        public const int GAM_ERR_LAST_CARD = 222;
        public const int GAM_ERROR_WRONG_DRAW = 223;
        public const int GAM_ERR_NO_SUCH_CARD = 224;
        public const int GAM_ERR_NOT_YOUR_TURN = 225;
        public const int CHA_SCC = 300;
        public const int CHA_ERR = 310;
    }
}
