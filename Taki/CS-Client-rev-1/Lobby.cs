﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace TakiClient
{
    public partial class Lobby : Form
    {
        public Lobby()
        {
            InitializeComponent();
        }

        private void Lobby_Load(object sender, EventArgs e)
        {
            this.AutoSize = true;
            this.RoomTable.AutoSize = true;

            label1.Text = "Hi, " + Global.username;

            this.RoomTable.AllowUserToAddRows = false;
            this.RoomTable.AllowUserToDeleteRows = false;
            this.RoomTable.AllowUserToOrderColumns = false;
            RefreshRoomList();
            Global.tm = new Timer();
            Global.tm.Interval = 10000;
            Global.tm.Tick += (s, args) => RefreshRoomList();
            Global.tm.Start();
        }

        private void RefreshRoomList()
        {
            byte[] msg = Encoding.ASCII.GetBytes(Global.MSG_START.ToString() + 
                Global.RM_ROOM_LIST.ToString() + Global.MSG_END);

            Room[] rooms;
            try
            {
                Global.s.Send(msg);
                msg = new byte[Global.CHUNK_SIZE];
                Global.s.Receive(msg);
            }
            catch (Exception e1)
            {
                MessageBox.Show("No connection. " + e1.GetType());
                this.Close();
            }
            
            Message message = new Message(msg);

            string[] room_list = message.GetMsgStrings();
            
            rooms = new Room[room_list.Length];

            for (int i = 0; i < rooms.Length; i++)
            {
                char[] delim = {','};
                string[] room_info = room_list[i].Split(delim);

                try
                {
                    rooms[i] = new Room(room_info[0], room_info[1], int.Parse(room_info[2]));
                }
                catch (Exception e1)
                {
                    return;
                }
            } 

            RoomTable.Rows.Clear();
            RoomTable.Refresh();

            for (int i = 0; i < rooms.Length; i++)
            {
                this.RoomTable.Rows.Add(rooms[i].GetName(), rooms[i].GetAdmin(),
                    rooms[i].GetCount().ToString() + "/4");
            }
        }

        private void RoomTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Global.tm.Stop();
            string smsg;
            try
            {
                smsg = Global.MSG_START.ToString() + Global.RM_JOIN_GAME.ToString() + Global.MSG_ARG_DELIM.ToString() +
                    RoomTable.Rows[e.RowIndex].Cells[1].Value.ToString() + Global.MSG_END;
            }
            catch (Exception e1)
            {
                Global.tm.Start();
                return;
            }

            byte[] msg = Encoding.ASCII.GetBytes(smsg);

            try
            {
                Global.s.Send(msg);
                msg = new byte[Global.CHUNK_SIZE];
                Global.s.Receive(msg);

            }
            catch (Exception e1)
            {
                MessageBox.Show("Error entering the room. " + e1.GetType() + ".");
                return;
            }

            Message message = new Message(msg);
            switch (message.GetCodeNum())
            {
                case Global.PGM_ERR_ROOM_FULL:
                    MessageBox.Show("The room is full.");
                    return;
                case Global.PGM_ERR_IN_GAME:
                    MessageBox.Show("Room is in game");
                    return;
            }

            char[] delim = { ',' };

            try
            {
                Global.players = message.GetMsgStrings()[0].Split(delim);
            }
            catch (Exception e1)
            {
                return;
            }

            this.Hide();

            RoomForm rf = new RoomForm();
            rf.FormClosed += (s, args) => this.Show();
            rf.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Global.tm.Stop();

            string smsg = Global.MSG_START.ToString() + Global.EN_LOGOUT.ToString() +
                Global.MSG_END;
            Global.s.Send(Encoding.ASCII.GetBytes(smsg));

            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Global.input = "SomeName";

            Input inp = new Input();

            inp.ShowDialog();

            string smsg = Global.MSG_START.ToString() + Global.RM_CREATE_GAME.ToString() +
                Global.MSG_ARG_DELIM.ToString() +
                Global.input +
                Global.MSG_ARG_DELIM + Global.MSG_END;
            Global.s.Send(Encoding.ASCII.GetBytes(smsg));

            byte[] msg = new byte[Global.CHUNK_SIZE];
            Global.s.Receive(msg);

            MessageBox.Show(Encoding.ASCII.GetString(msg));
        }
    }
}
