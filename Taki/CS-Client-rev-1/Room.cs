﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakiClient
{
    class Room
    {
        private string Name;
        private string Admin;
        private int Count;

        public Room(string name, string admin, int count)
        {
            Name = name;
            Admin = admin;
            Count = count;
        }

        public string GetName()
        {
            return Name;
        }

        public string GetAdmin()
        {
            return Admin;
        }

        public int GetCount()
        {
            return Count;
        }
    }
}
