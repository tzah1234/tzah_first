﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakiClient
{
    class Message
    {
        private int codeNum;
        private string[] msgStr;

        public Message(byte[] msg)    
        {
            string str = Encoding.UTF8.GetString(msg);
            str = str.Remove(0, 1);
            str = str.Remove(str.IndexOf('\0'), str.Length - str.IndexOf('\0'));
            char[] delims = {'|'};
            string[] arr = str.Split(delims);
            
            codeNum = int.Parse(arr[0]);
            arr = arr.Where(val => val != "" && val[0] != '\0').ToArray();

            var l = new List<string>(arr);
            l.RemoveAt(0);
            msgStr = l.ToArray();
        }

        public int GetCodeNum()
        {
            return codeNum;
        }

        public string[] GetMsgStrings()
        {
            return msgStr;
        }
    }
}
