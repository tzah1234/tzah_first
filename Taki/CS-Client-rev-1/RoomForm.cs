﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakiClient
{
    public partial class RoomForm : Form
    {
        public RoomForm()
        {
            InitializeComponent();
        }

        private void RoomForm_Load(object sender, EventArgs e)
        {
            byte[] exit_msg = Encoding.ASCII.GetBytes(Global.MSG_START.ToString() +
                Global.RM_LEAVE_GAME.ToString() + Global.MSG_END);
            this.FormClosed += (s, args) => Global.s.Send(exit_msg);
            this.FormClosed += (s, args) => Global.tm.Start();
        }
    }
}
