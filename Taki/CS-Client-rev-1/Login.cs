﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace TakiClient
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            Global.s = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            this.FormClosed += (s, args) => Global.s.Close();

            bool isConnected = false;
            while (!isConnected)
            {
                try
                {
                    Global.s.Connect("127.0.0.1", 7777);
                    isConnected = true;
                }
                catch (SocketException e1)
                {
                    MessageBox.Show(e1.ErrorCode.ToString(), "Error");
                }
                catch (Exception e1)
                {
                    MessageBox.Show("Exception");
                }
            }
        }

        private bool isFilled()
        {
            return (UsernameTextBox.Text != String.Empty && PasswordTextBox.Text != String.Empty);
        }

        private void RegisterOrLogin(bool register)
        {
            if (isFilled())
            {
                Global.username = UsernameTextBox.Text;
                string s_msg = Global.MSG_START.ToString() +(register ? Global.EN_REGISTER.ToString() : Global.EN_LOGIN.ToString()) + 
                    Global.MSG_ARG_DELIM + UsernameTextBox.Text + Global.MSG_ARG_DELIM.ToString() + 
                    PasswordTextBox.Text + Global.MSG_END;

                byte[] msg = Encoding.ASCII.GetBytes(s_msg);
                
                Global.s.Send(msg);

                msg = new byte[Global.CHUNK_SIZE];
                Global.s.Receive(msg);
                
                Message message = new Message(msg);

                int statusCode = message.GetCodeNum();

                switch (statusCode)
                {
                    case Global.PGM_ERR_LOGIN:
                        MessageBox.Show("Login details are incorrect OR you are already connected.", "Error");
                        return;
                    case Global.PGM_ERR_NAME_TAKEN:
                        MessageBox.Show("The name " + message.GetMsgStrings()[0] + " is taken.", "Error");
                        return;
                    case Global.PGM_ERR_INFO_TOO_LONG:
                        MessageBox.Show("Your username is too long", "Error");
                        return;
                }

                this.Hide();

                Lobby lobby = new Lobby();
                lobby.FormClosed += (s, args) => this.Show();
                lobby.Show();
            }
            else
            {
                MessageBox.Show("Fill the form first.", "Alert");
            }
        }

        private void Register_Click(object sender, EventArgs e)
        {
            RegisterOrLogin(true);
        }

        private void Login_Click(object sender, EventArgs e)
        {
            RegisterOrLogin(false);
        }
    }
}
